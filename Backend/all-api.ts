import express from 'express'
import bodyparser from 'body-parser';
import AWS from 'aws-sdk';
import cors from 'cors';

const port = 8001;
const app = express();
app.listen(port,()=>{
    console.log("The server is running on loaclhost 8001");
})

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended : true }));
app.use(cors());

AWS.config.update({
  region: "us-west-2",
  endpoint: "http://localhost:8000"
});

var docClient = new AWS.DynamoDB.DocumentClient();

app.get('/',(req,res)=>{
    res.send({response:"Yeah the server is running"});
})

var table = "todo_app_list";
var user = " ";
app.post('/GetUser',async (req,res)=>{
    user = req.body.username;

    var task = "Manage your todo app";
    const CreateUser = () => 
    {
        var params2 = {
            TableName:table,
            Item:{
                "username": user,
                "todo": task,
                "stat" : 2
            }
        };
        
        console.log("Adding a new user...");
        docClient.put(params2, function(err:any, data:any) {
            if (err) {
                console.error("Unable to add user. Error JSON:", JSON.stringify(err, null, 2));
            } else {
                console.log("Added user:", JSON.stringify(data, null, 2));
            }
        });    
    }

    var params = {
        TableName: table,
        Key:{
            "username": user,
            "todo": task,
        }
    };

    await docClient.get(params, function(err:any, data:any) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null, 2));
        } 
        else {
            console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
            if(typeof data.Item !== "undefined")
            {   
                res.send("The user already exists")
            }
            else 
            {
                res.send("Creating new user")
                CreateUser();
                return;
            }
        }
    });

});

app.get('/LoadTodoList',async(req,res)=>{
    
    var params={
        TableName : table,
        ProjectionExpression: 'todo,stat',
        KeyConditionExpression:"#username= :username",
        ExpressionAttributeNames:{
            "#username": "username",
        },
        ExpressionAttributeValues:{
            ":username": user,
        },
    };
    await docClient.query(params, (err, data) => {
        if (err) {
            console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
        }
        else {
            console.log("Query succeeded.",JSON.stringify(data,null,2));
            res.send(data.Items);
        }
    })
});

app.post('/UpdateTodo',(req,res)=>{

    var todo = req.query.todo;
    var params = {
        TableName: table,
        Key:{
            "username": user,
            "todo" : todo,
        },
        UpdateExpression: "set stat = :stat",
        ConditionExpression: "stat= :s",
        ExpressionAttributeValues:{
            ":s":0,
            ":stat": 1
        },
        ReturnValues:"UPDATED_NEW"
    };

    console.log("Updating status...");
    docClient.update(params, function(err: any, data: any) {
        if (err) {
            console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
        }
    });
    res.send("completed the todo and updating the database...........");
});

app.put('/AddTodo',async (req,res)=>{
    var task = req.body.todo;

    var params = {
        TableName: table,
        Item:{
            "username": user,
            "todo": task,
            "stat": 0,
        },
    };
    console.log("Updating the todo list...");
    await docClient.put(params, function(err: any, data: any) {
        if (err) {
            console.error("Unable to add task. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("Add task completed:", JSON.stringify(data, null, 2));
        }
    });
    res.send("Adding Todo ......");
});




            