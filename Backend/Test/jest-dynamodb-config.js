module.exports = {
    tables: [
      {
        TableName: `Todolist2`,
        KeySchema: [{AttributeName: 'user_name', KeyType: 'HASH'},{AttributeName: 'todo', KeyType: 'SORT'},],
        AttributeDefinitions: [{AttributeName: 'user_name', AttributeType: 'S'},{AttributeName: 'todo', AttributeType: 'S'}],
        ProvisionedThroughput: {ReadCapacityUnits: 5, WriteCapacityUnits: 5},
      },
    ],
  };