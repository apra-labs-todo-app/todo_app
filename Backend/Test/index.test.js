const {DocumentClient} = require('aws-sdk/clients/dynamodb');

const isTest = process.env.JEST_WORKER_ID;
const config = {
  convertEmptyValues: true,
  ...(isTest && {
    endpoint: 'localhost:8000',
    sslEnabled: false,
    region: 'local-env',
  }),
};

const ddb = new DocumentClient(config);

it('should insert item into table', async () => {
    await ddb
      .put({TableName: 'Todolist2', Item: {user_name: 'Varma', task: 'Complete to do test'}})
      .promise();
  
    const {Item} = await ddb.get({TableName: 'Todolist2', Key: {user_name: 'Varma'}}).promise();
  
    expect(Item).toEqual({
      user_name: 'Varma',
      task: 'Complete to do test',
    });
  });