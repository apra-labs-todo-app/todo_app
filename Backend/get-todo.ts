import express from 'express'
import bodyparser from 'body-parser';
import AWS from 'aws-sdk';


const port = 8001;
const app = express();
app.listen(port,()=>{
    console.log("The server is running on loaclhost 8001");
})


app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended : true }));


AWS.config.update({
  region: "us-west-2",
  //endpoint: "http://localhost:8000"
});

var docClient = new AWS.DynamoDB.DocumentClient();

app.get('/',(req,res)=>{
    res.send("Yeah the server is running");
})

var table = "todo_app_list";

app.get('/LoadTodoList',async(req,res)=>{

    var user = req.query.username;
    var choice=req.query.stat ;
    
    var params={
        TableName : table,
        ProjectionExpression: 'todo,stat',
        KeyConditionExpression:"#username= :username",
        ConditionExpression: "#stat= :stat",
        ExpressionAttributeNames:{
            "#username": "username",
            '#stat':'stat',
        },
        ExpressionAttributeValues:{
            ":username": user,
            ':stat' : choice,
        },
    };
    docClient.query(params, (err, data) => {
        if (err) {
            console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
        }
        else {
            console.log("Query succeeded.");
            // data.Items.forEach(function (item) {
            //     console.log(
            //         item.todo + ": ", item.stat);
            // });
        }
    })
    res.send("Fetched the todo list succesfully "+ user +" "+ choice);
});

app.post('/UpdateTodo',(req,res)=>{

    var user = req.query.username;
    var todo = req.query.todo;
    var params = {
        TableName: table,
        Key:{
            "username": user,
            "todo" : todo,
        },
        UpdateExpression: "set stat = :stat",
        ConditionExpression: "stat= :s",
        ExpressionAttributeValues:{
            ":s":0,
            ":stat": 1
        },
        ReturnValues:"UPDATED_NEW"
    };

    console.log("Updating status...");
    docClient.update(params, function(err: any, data: any) {
        if (err) {
            console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
        }
    });
    res.send("completed the todo and updating the database...........");
});
               