import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import './index.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import RouterApp from './router-app';


ReactDOM.render(
  < Router>
  <RouterApp />
  </Router>,
  document.getElementById('root') 
);
