import React from 'react';
import './index.css';
import {
  Button,
  Col,
  Container,
  Dropdown,
  Jumbotron,
  ListGroup,
  Row,
  SplitButton,
} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import { Link } from 'react-router-dom';
import axios from 'axios';

interface Iprops {}

interface TodoItem {
  todo: string;
  stat: number;
}

interface Istate {
  flag: boolean;
  status: number;
  todolist: TodoItem[];
  templist : TodoItem[];
}

export class TodoListPage extends React.Component<Iprops, Istate> {
  state: Istate;
  constructor(props: Iprops) {
    super(props);
    this.state = {
      flag: true,
      status: 2,
      todolist: [],
      templist:[],
    };
  }

  login = async () => {
    if (this.state.flag) {
      this.setState({ flag: false });
      await axios
        .get(`http://localhost:8001/LoadTodoList`)
        .then(res => {
          const todolist = res.data;
          this.setState({ todolist: todolist });
        })
        .catch(error => {
          console.log(error);
        });
      var newList: TodoItem[];
      newList = this.state.todolist.filter(
        item => item.stat !== 2,
      );
      this.setState({ todolist: newList });
      newList = this.state.todolist.filter(
        item => item.stat !== this.state.status,
      );
      this.setState({ templist: newList });
      console.log('working');
    } else return;
  };

  HandleSelect = (choice: number) => {
    this.setState({ status: choice });
  };

  StatusUpdate = async (i: number) => {
    try {
      var body = {
        todo: `${this.state.templist[i].todo}`,
      };
      await axios
        .post(`http://localhost:8001/UpdateTodo`, body)
        .then(res => res.data)
        .catch(error => {
          console.log(error);
          return;
        });
    } catch (error) {
      console.log('Technical error');
      return;
    }
    this.setState({ flag: true });
  };

  render() {
    this.login();
    return (
      <div style={{ textAlign: 'center' }}>
        <h1>Welcome to your To-Do list page</h1>
        <SplitButton
          menuAlign="right"
          title="FILTER TASKS"
          variant="secondary"
          id="dropdown-for-status"
        >
          <Dropdown.Item eventKey="1" onClick={() => this.HandleSelect(0)}>
            Complete
          </Dropdown.Item>
          <Dropdown.Item eventKey="2" onClick={() => this.HandleSelect(1)}>
            Incomplete
          </Dropdown.Item>
        </SplitButton>

        <Jumbotron>
          <Container>
            <Row noGutters md={12}>
              <Col md={12}>
                <ListGroup>
                  {this.state.todolist
                    .filter(item => item.stat !== this.state.status)
                    .map((item: TodoItem, i) => {
                      return (
                        <ListGroup.Item key={i}>
                          {i + ' : ' + item.todo}
                          <Button
                            className="btn btn-lg float-right"
                            variant={item.stat ? 'success' : 'danger'}
                            onClick={() => this.StatusUpdate(i)}
                          >
                            {item.stat ? ' Completed ' : 'Incomplete'}
                          </Button>
                        </ListGroup.Item>
                      );
                    })}
                </ListGroup>
              </Col>
            </Row>
          </Container>
        </Jumbotron>

        <div>
          <form>
            <Link to="/">
              <Button variant="success" size="lg">
                Back
              </Button>
            </Link>
            <p>{this.state.todolist.length}</p>
            <Link to="/add-todo">
              <Button variant="success" size="lg">
                Add
              </Button>
            </Link>
          </form>
        </div>
      </div>
    );
  }
}
