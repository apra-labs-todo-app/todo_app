import * as React from 'react';
import { Link } from 'react-router-dom';
import './index.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import { Button, Jumbotron } from 'react-bootstrap';
import axios from 'axios';

interface Iprops{}

interface Istate
{
  username :string;
  status: string
}

export class HomePage extends React.Component<Iprops,Istate>
{
    state:Istate;
    constructor(props: Iprops)
    {
    super(props);
    this.state ={ 
        username : '',
        status : '',
    };
    }

    HandlingUsername = (event: { target: { value: any; }; }) => {
        this.state.username = event.target.value.toLowerCase();
    }
    login =()=>{
        var body = {
            "username" : `${this.state.username}`,
        }
        axios.post('http://localhost:8001/GetUser',body)
        .then(response => {
            console.log(response.data);
        })
        .catch( error => {
            console.log(error);
        })
    }
    Success = async() =>{
           
        if(this.state.username === '')
        {alert("Enter a valid username");}
        else
        {
            this.login();
            alert("Welcome " + this.state.username + "\nYou are being directed to the next page.......");
        }
    }

    render() {
    return (
        <Jumbotron>
        <div style = {{textAlign:"center"}} >
        <h1>Welcome to your Todo App</h1>
        <form>
        <input type='text' onChange={this.HandlingUsername} style={{width: "500px"}} placeholder="User Name" required />
        </form>
        <form >
        <Link to ="/todo-list">
        <Button variant="success" onClick={this.Success} size="lg">Submit</Button>
        </Link>
        </form>
        </div>
        </Jumbotron>
    );
    }
}
