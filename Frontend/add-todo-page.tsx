
import React from 'react';
import './index.css';
import { Link } from 'react-router-dom';
import { Button, Jumbotron } from 'react-bootstrap';
import axios from 'axios';

interface Iprops{}
interface Istate{
    todo: string;
}
export class AddTodoPage extends React.Component<Iprops,Istate>
{
    state:Istate;
    constructor(props:Iprops)
    {
        super(props);
        this.state={ todo :' '};
    }

    HandlingTask = (event: { target: { value: any; }; }) => {
        this.state.todo = event.target.value;
    }
    login =()=>{
        var body = {
            "todo" : `${this.state.todo}`,
        }
        axios.put('http://localhost:8001/AddTodo',body)
        .then(response => {
            alert(response.data);
        })
        .catch( error => {
            console.log(error);
        })
    }
    Success = () =>{
        if(this.state.todo ===' ')
        {
            alert("Enter a task");
        }
        else
        {
            this.login();
            alert("List updated");
        }
    }

    render() {
    return (
        <Jumbotron>
        <div style = {{textAlign:"center"}}>
            <h1>Just add your To-do</h1>
            <form>
                <input type='text' onChange={this.HandlingTask} placeholder="Add your task here" required></input>
            </form>

            <form>
            <Link to="todo-list"> 
            <Button variant="primary" size="lg"> Cancel </Button>
            </Link>
            <h1>         </h1>
            <Link to="todo-list">
            <Button variant="success" onClick={this.Success} size="lg">Submit</Button>
            </Link> 
            </form>
        </div>
        </Jumbotron>
    );
    }
}

