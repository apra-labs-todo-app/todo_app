import { Route, Switch } from "react-router-dom";

import Home from "./home";
import TodoListPage from "./todo-list-page";
import AddTodoPage from "./add-todo-page";
import PageNotFound from "./page-not-found";

function RouterApp() {
  return (
    <div className="container-fluid">
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/todo-list" component={TodoListPage} />
        <Route exact path="/add-todo" component={AddTodoPage} />
        <Route component={PageNotFound} />
      </Switch>
    </div>
  );
}

export default RouterApp;
